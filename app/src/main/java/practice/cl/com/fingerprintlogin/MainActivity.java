package practice.cl.com.fingerprintlogin;

import android.hardware.fingerprint.FingerprintManager;
import android.os.Bundle;
import android.os.CancellationSignal;
import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.spec.ECGenParameterSpec;

public class MainActivity extends AppCompatActivity {

    private KeyPairGenerator keyPairGenerator;
    private static String KEY_NAME = "KEY_NAME";
    private Signature signature;
    private FingerprintManager.CryptoObject cryptoObjectToStore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findViewById(R.id.btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                try {
                    startAuthentication();
                } catch (CertificateException e) {
                    e.printStackTrace();
                } catch (NoSuchAlgorithmException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (KeyStoreException e) {
                    e.printStackTrace();
                } catch (InvalidKeyException e) {
                    e.printStackTrace();
                } catch (UnrecoverableKeyException e) {
                    e.printStackTrace();
                }
            }
        });


        try {
            generateKeyPair();
        } catch (NoSuchProviderException | InvalidAlgorithmParameterException | NoSuchAlgorithmException e) {
            e.printStackTrace();
        }


        try {
            getKeys();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * generate assymetric key pair
     *
     * @throws NoSuchProviderException  ex
     * @throws NoSuchAlgorithmException ex
     */
    private void generateKeyPair() throws NoSuchProviderException, NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        keyPairGenerator = KeyPairGenerator.getInstance(KeyProperties.KEY_ALGORITHM_EC, "AndroidKeyStore");
        keyPairGenerator.initialize(
                new KeyGenParameterSpec.Builder(KEY_NAME,
                        KeyProperties.PURPOSE_SIGN)
                        .setDigests(KeyProperties.DIGEST_SHA256)
                        .setAlgorithmParameterSpec(new ECGenParameterSpec("secp256r1"))
                        .setUserAuthenticationRequired(true)
                        .build());
        keyPairGenerator.generateKeyPair();
    }

    /**
     * get assymetric key pair
     */
    private void getKeys() throws KeyStoreException, CertificateException, NoSuchAlgorithmException, IOException, UnrecoverableKeyException {
        KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        PublicKey publicKey =
                keyStore.getCertificate(MainActivity.KEY_NAME).getPublicKey();
        Log.e("public key : ", publicKey.toString());

        keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        PrivateKey key = (PrivateKey) keyStore.getKey(KEY_NAME, null);
        Log.e("private key : ", key.toString());

    }

    private void sendPublicToServer() {
        /**
         * send public key to server before this
         *
         */
        Signature signature = cryptoObjectToStore.getSignature();
// Include a client nonce in the transaction so that the nonce is also signed
// by the private key and the backend can verify that the same nonce can't be used
// to prevent replay attacks.
        Transaction transaction = new Transaction("user", "access token");
        try {
            signature.update(transaction.toByteArray());
            Log.e("signing", "bytes");
            byte[] sigBytes = signature.sign();
            // Send the transaction and signedTransaction to the dummy backend

            Log.e("data sent to server : ", sigBytes.toString());

        } catch (SignatureException e) {
            throw new RuntimeException(e);
        }
    }

    private void startAuthentication() throws CertificateException, NoSuchAlgorithmException
            , IOException, KeyStoreException, InvalidKeyException, UnrecoverableKeyException {
        signature = Signature.getInstance("SHA256withECDSA");
        KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
        keyStore.load(null);
        PrivateKey key = (PrivateKey) keyStore.getKey(KEY_NAME, null);
        signature.initSign(key);
        FingerprintManager.CryptoObject cryptObject = new FingerprintManager.CryptoObject(signature);

        CancellationSignal cancellationSignal = new CancellationSignal();
        FingerprintManager fingerprintManager =
                this.getSystemService(FingerprintManager.class);
        fingerprintManager.authenticate(cryptObject, cancellationSignal, 0, new AuthListener(), null);
    }


    /**
     * listener
     */
    class AuthListener extends FingerprintManager.AuthenticationCallback {

        AuthListener() {
            super();
            Log.e("executing ", "AuthListener");
        }

        @Override
        public void onAuthenticationError(final int errorCode, final CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);
            Log.e("executing ", "onAuthenticationError");
            Log.e("executing ", "errString : " + errString + "  errorCode :  " + errorCode);
        }

        @Override
        public void onAuthenticationHelp(final int helpCode, final CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
            Log.e("executing ", "onAuthenticationHelp");
            Log.e("executing ", "helpString : " + helpString + "  helpCode :  " + helpCode);
        }

        @Override
        public void onAuthenticationSucceeded(final FingerprintManager.AuthenticationResult result) {
            super.onAuthenticationSucceeded(result);
            Log.e("success", "onAuthenticationSucceeded");
            cryptoObjectToStore = result.getCryptoObject();
            sendPublicToServer();
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
            Log.e("failed", "onAuthenticationFailed");
        }
    }


    /*
    dummy object
     */
    private class Transaction implements Serializable {
        String email;
        String token;

        Transaction(String email, String token) {
            this.email = email;
            token = token;
        }

        byte[] toByteArray() {
            byte[] byteArray = new byte[0];
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutput out = null;
            try {
                out = new ObjectOutputStream(bos);
                out.writeObject(this);
                out.flush();
                byteArray = bos.toByteArray();

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {
                    bos.close();
                } catch (IOException ex) {
                    // ignore close exception
                }
            }
            Log.e("out", "byte converted");
            return byteArray;
        }
    }
}
